package uk.co.rullion;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TaskParserTest {

    @Test
    public void getTaskOrderNoTasks() {
        TaskParser taskParser = new TaskParser();

        String taskOrder = taskParser.getTaskOrder();
        assertEquals("[]", taskOrder);
    }

    @Test
    public void getTaskOrderNoDependencies() {
        TaskParser taskParser = new TaskParser();

        taskParser.addTask("a");
        taskParser.addTask("b");
        String taskOrder = taskParser.getTaskOrder();
        assertEquals("[a,b]", taskOrder);
    }


    @Test
    public void getTaskOrderSingleDependency() {
        TaskParser taskParser = new TaskParser();
        taskParser.addTask("a");
        taskParser.addTask("b");

        taskParser.addDependency("a=>b");

        String taskOrder = taskParser.getTaskOrder();
        assertEquals("[b,a]", taskOrder);
    }

    @Test
    public void getTaskOrderTwoIndependentDependencies() {
        TaskParser taskParser = new TaskParser();
        taskParser.addTask("a");
        taskParser.addTask("b");
        taskParser.addTask("c");
        taskParser.addTask("d");

        taskParser.addDependency("a=>b");
        taskParser.addDependency("c=>d");

        String taskOrder = taskParser.getTaskOrder();
        assertEquals("[b,a,d,c]", taskOrder);
    }

    @Test
    public void getTaskOrderChainedDependency() {
        TaskParser taskParser = new TaskParser();
        taskParser.addTask("a");
        taskParser.addTask("b");
        taskParser.addTask("c");

        taskParser.addDependency("a=>b");
        taskParser.addDependency("b=>c");

        String taskOrder = taskParser.getTaskOrder();
        assertEquals("[c,b,a]", taskOrder);
    }

    @Test
    public void getTaskOrderCyclicDependency() {
        TaskParser taskParser = new TaskParser();
        taskParser.addTask("a");
        taskParser.addTask("b");
        taskParser.addTask("c");

        taskParser.addDependency("a=>b");
        taskParser.addDependency("b=>c");
        taskParser.addDependency("c=>a");

        String taskOrder = taskParser.getTaskOrder();
        assertEquals("Error - this is a cyclic dependency", taskOrder);
    }

}