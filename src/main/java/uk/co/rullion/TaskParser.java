package uk.co.rullion;

import org.apache.commons.lang.StringUtils;
import uk.co.rullion.datastructure.Dependency;
import uk.co.rullion.exceptions.CyclicDependencyException;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TaskParser {

    private List<String> taskResults = new ArrayList<>();
    private List<String> tasks = new ArrayList<>();
    private List<Dependency> dependencies = new ArrayList<>();


    public void addTask(String task)
    {
        tasks.add(task);
    }

    public void addDependency(String dependencyString)
    {
        if (!StringUtils.isBlank(dependencyString)) {
            if (!dependencyString.contains("=>"))
            {
                throw new IllegalArgumentException("Supplied dependency string "+dependencyString+" is not well formed. Dependencies must be in the format \"a=>b\"");
            }
            String[] components = dependencyString.split("=>");
            Dependency dependency = new Dependency(components[0].trim(),components[1].trim());
            dependencies.add(dependency);
        }
    }

    public String getTaskString()
    {
        return "["+String.join(",",tasks)+"]";
    }

    public String getDependencyString()
    {
        String dependencyString = dependencies.stream().map(dependency -> dependency.getDependent()+"=>"+dependency.getDependency()).collect(Collectors.joining(","));

        return "["+dependencyString+"]";
    }

    public String getTaskOrder()
    {
        if (tasks.isEmpty())
        {
             return "[]";
        }

        for (String task:tasks)
        {
            if (!taskResults.contains(task))
            {
                Deque<String> localStack = new ArrayDeque<>();
                localStack.add(task);
                try {
                    processDependencies(task, localStack);
                } catch (CyclicDependencyException e) {
                    return "Error - this is a cyclic dependency";
                }
                taskResults.addAll(localStack);
            }
        }


        String resultString = String.join(",", taskResults);
        return "["+resultString+"]";
    }

    private void processDependencies(String task, Deque<String> localStack) throws CyclicDependencyException {
        for (Dependency dependency:dependencies)
        {
            if (dependency.getDependent().equals(task))
            {
                String dependencyValue = dependency.getDependency();
                if (!taskResults.contains(dependencyValue))
                {
                    if (localStack.contains(dependencyValue))
                    {
                        throw new CyclicDependencyException();
                    }

                    localStack.push(dependencyValue);
                    processDependencies(dependencyValue,localStack);
                }
            }
        }
    }


}
