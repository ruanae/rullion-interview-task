package uk.co.rullion.datastructure;

public class Dependency {

    private final String dependent;
    private final String dependency;

    public Dependency(String dependent, String dependency) {
        this.dependent = dependent;
        this.dependency = dependency;
    }

    public String getDependent() {
        return dependent;
    }

    public String getDependency() {
        return dependency;
    }
}
