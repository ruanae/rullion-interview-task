package uk.co.rullion;

import org.apache.commons.cli.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        // create the command line parser
        CommandLineParser parser = new DefaultParser();

        Options options = new Options();

        options.addOption("t", true, "A comma separate list of tasks");
        options.addOption("d", true, "A comma separate list of dependencies in the format \"a=>b\"");
        options.addOption("h",false,"print this message");



        TaskParser taskParser = new TaskParser();

        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("h"))
            {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "java -jar rullion-interview-2018-1.0-SNAPSHOT-shaded.jar", options );
            }
            else {
                // validate that block-size has been set
                if (line.hasOption("t")) {
                    String tasks = line.getOptionValue("t");
                    String[] splitTasks = tasks.split(",");
                    for (String task : splitTasks) {
                        taskParser.addTask(task);
                    }
                }

                if (line.hasOption("d")) {
                    String dependencies = line.getOptionValue("d");
                    String[] splitDependencies = dependencies.split(",");
                    for (String dependency : splitDependencies) {
                        taskParser.addDependency(dependency);
                    }
                }

                System.out.println("tasks: " + taskParser.getTaskString());
                System.out.println("dependencies: " + taskParser.getDependencyString());
                System.out.println("result: " + taskParser.getTaskOrder());
            }

        } catch (ParseException exp) {
            System.err.println("Unexpected exception:" + exp.getMessage());
        }

    }
}
