# Rullion Interview Task

This is the implementation of the task parser which should illustrate the code to handle the acceptance criteria defined in the task outline.

There is a JUnit test case for each of the acceptance criteria listed.

Building the application
This application uses Maven as its build tool. The result of the maven build will be a shaded jar file i.e. a executable jar containing all the necessary dependencies. The maven command to build the project is

mvn clean package

This will build the jar file rullion-interview-2018.1.0-SNAPSHOT-shaded.jar in the folder target


Running the application
The application can be run from the command line using the command
java -jar rullion-interview-2018-1.0-SNAPSHOT-shaded.jar 

There are three command line arguments used by this application 
 -h which will print out a basic usage guide

-t <arg>   A comma separate list of tasks
-d <arg>   A comma separate list of dependencies in the format "a=>b"

So if we wanted to run the application with the tasks a and b we would call

 java -jar rullion-interview-2018-1.0-SNAPSHOT-shaded.jar -t a,b

And if we wanted to call the application with the tasks a,b and c with a dependency between a and b we would call

java -jar rullion-interview-2018-1.0-SNAPSHOT-shaded.jar -t a,b -d a=\>b

Note - when running this via Cygwin on Windows it was necessary to escape the > character which is why this is preceded by a \. This may be required on other systems.


The output of the application should print to the command line in the format
java -jar rullion-interview-2018-1.0-SNAPSHOT-shaded.jar -t a,b,c,d -d a=\>b,c=\>d


tasks: [a,b,c,d]
dependencies: [a => b,c => d]
result: [b,a,d,c]
